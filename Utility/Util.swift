//
//  Util.swift
//  SmartMobe
//
//  Created by Roshan on 7/12/19.
//  Copyright © 2019 Roshan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Util {
    
    class func dropShadow(view: UIView, color: UIColor, opacity: Float, offSet: CGSize, radius: CGFloat, scale: Bool) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = opacity
        view.layer.shadowOffset = offSet
        view.layer.shadowRadius = radius
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }

    class func showAlert(title:String, message:String, view:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }


    
}


