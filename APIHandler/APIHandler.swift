//
//  APIHandler.swift
//  SmartMobe
//
//  Created by Studio on 7/12/19.
//  Copyright © 2019 Roshan. All rights reserved.
//

import UIKit
import Alamofire

class APIHandler: NSObject {
    static let shared = APIHandler()
    
    func productFetch(query: String, success:@escaping (_ topTensArray: [TopTens]) -> (), failure: @escaping (_ message:Error) -> ()){
        var url = URLComponents(string: PRODUCT_URL)!
        url.queryItems = [
            URLQueryItem(name: "query", value: query)
        ]
        Alamofire.request(url, method: .get, encoding : URLEncoding.default).responseJSON { response in
            guard (response.result.value as? [String:Any]) != nil else{
                print("Error: \(String(describing: response.result.error))")
                failure((response.result.error! as Error))
                return
            }
            if let JSON = response.result.value {
                print("JSON", JSON)
                let dict = JSON as! [String: Any]
                let dataArray = dict["images"] as? [[String: Any]]
                var topTensArray: Array = [TopTens]()
                if let topTensArr = dataArray{
                    for data in topTensArr{
                        if let id = data["id"],
                            let imageUrl = data["url"],
                            let largeUrl = data["large_url"]{
                            let topTens: TopTens = TopTens()
                            topTens.id = id as! Int
                            topTens.imageUrl = imageUrl as? String
                            topTens.largeUrl = largeUrl as? String
                            topTensArray.append(topTens)
                        }
                    }
                }
                success(topTensArray)
            }
        }
    }
    
    func latestFetch(success:@escaping (_ topTensArray: [TopTens]) -> (), failure: @escaping (_ message:Error) -> ()){
        Alamofire.request(LATEST_URL, method: .get, encoding : URLEncoding.default).responseJSON { response in
            guard (response.result.value as? [String:Any]) != nil else{
                print("Error: \(String(describing: response.result.error))")
                failure((response.result.error! as Error))
                return
            }
            if let JSON = response.result.value {
                print("JSON", JSON)
                let dict = JSON as! [String: Any]
                let dataArray = dict["images"] as? [[String: Any]]
                var topTensArray: Array = [TopTens]()
                if let topTensArr = dataArray{
                    for data in topTensArr{
                        if let id = data["id"],
                            let imageUrl = data["url"],
                            let largeUrl = data["large_url"]{
                            let topTens: TopTens = TopTens()
                            topTens.id = id as! Int
                            topTens.imageUrl = imageUrl as? String
                            topTens.largeUrl = largeUrl as? String
                            topTensArray.append(topTens)
                        }
                    }
                }
                success(topTensArray)
            }
        }
    }
    
    
    
    
}
