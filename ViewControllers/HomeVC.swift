//
//  HomeVC.swift
//  SmartMobe
//
//  Created by Roshan Rijal on 7/12/19.
//  Copyright © 2019 Roshan Rijal. All rights reserved.
//

import UIKit
import APESuperHUD

struct ScreenSize{
    static let screenWidth         = UIScreen.main.bounds.size.width
    static let screenHeight        = UIScreen.main.bounds.size.height
    static let screenMaxLength     = max(ScreenSize.screenWidth, ScreenSize.screenHeight)
    static let screenMinLength     = min(ScreenSize.screenWidth, ScreenSize.screenHeight)
}

class HomeVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    @IBOutlet weak var lblNoProductInfo: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collectionTopTens: UICollectionView!
    @IBOutlet weak var btnSearch: UIButton!
    
    var topTensArray: Array = [TopTens]()
    var selectedTopTens: TopTens = TopTens()
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        HUDAppearance.backgroundColor = .lightGray
        HUDAppearance.iconSize = CGSize(width: 48, height: 48)
        HUDAppearance.messageFont = UIFont.systemFont(ofSize: 14.0)
        
        lblNoProductInfo.isHidden = true
    }
    
    fileprivate func loadData() {
        if Util.isConnectedToInternet(){
            APESuperHUD.show(style: .loadingIndicator(type: .standard), title: nil, message: "Loading...")
            APIHandler.shared.latestFetch(success: { (topTensArray) in
                APESuperHUD.dismissAll(animated: true)
                self.topTensArray = topTensArray
                if self.topTensArray.count == 0{
                    self.lblNoProductInfo.isHidden = false
                    self.collectionTopTens.isHidden = true
                }else{
                    self.lblNoProductInfo.isHidden = true
                    self.collectionTopTens.isHidden = false
                    self.collectionTopTens.reloadData()
                }
                self.collectionTopTens.reloadData()
            }) { (failure) in
                APESuperHUD.dismissAll(animated: true)
                Util.showAlert(title: "Error", message: failure.localizedDescription, view: self)
            }
        }else{
            Util.showAlert(title: "Oops", message: "No internet connection", view: self)
        }
    }
    
    fileprivate func isValid()->Bool {
        if self.txtSearch.text == ""{
            self.txtSearch.attributedPlaceholder = NSAttributedString(string: "Product Name Required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            return false
        }else{
            self.txtSearch.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        return true
    }
    
    func pushDetailsVC(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        vc.selectedTopTens = self.selectedTopTens
        self.present(vc, animated: true, completion: nil)
    }
    
    fileprivate func performSearch(){
        if Util.isConnectedToInternet(){
            APESuperHUD.show(style: .loadingIndicator(type: .standard), title: nil, message: "Searching...")
            APIHandler.shared.productFetch(query: txtSearch.text!, success: { (topTensArray) in
                APESuperHUD.dismissAll(animated: true)
                self.topTensArray = topTensArray
                if self.topTensArray.count == 0{
                    self.lblNoProductInfo.isHidden = false
                    self.collectionTopTens.isHidden = true
                }else{
                    self.lblNoProductInfo.isHidden = true
                    self.collectionTopTens.isHidden = false
                    self.collectionTopTens.reloadData()
                }
                self.txtSearch.text = ""
                self.txtSearch.resignFirstResponder()
            }) { (failure) in
                APESuperHUD.dismissAll(animated: true)
                Util.showAlert(title: "Error", message: failure.localizedDescription, view: self)
                self.txtSearch.text = ""
                self.txtSearch.resignFirstResponder()
            }
        }else{
            Util.showAlert(title: "Oops", message: "No internet connection", view: self)
        }
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnSearchItemsAction(_ sender: Any) {
        performSearch()
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        configureView()
        txtSearch.delegate = self
        txtSearch.returnKeyType = .search
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: UICollectionView Delegate Methods

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topTensArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionTopTens", for: indexPath as IndexPath) as! CollectionTopTens
        let topTens:TopTens = self.topTensArray[indexPath.item]

        cell.loadCell(topTens: topTens)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize (width: (ScreenSize.screenWidth) / 2.1, height: (ScreenSize.screenWidth) / 2.1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedTopTens = self.topTensArray[indexPath.item]
        pushDetailsVC()
    }
    
    
    // MARK: -
    // MARK: UITextField Delegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        performSearch()
        return true
    }
    
    

}
