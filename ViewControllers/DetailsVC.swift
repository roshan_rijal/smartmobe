//
//  DetailsVC.swift
//  SmartMobe
//
//  Created by Roshan Rijal on 7/12/19.
//  Copyright © 2019 Roshan Rijal. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController, UIScrollViewDelegate {

    var selectedTopTens: TopTens = TopTens()

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgPhoto: UIImageView!
    
    // MARK: -
    // MARK: Private Utility Methods

    fileprivate func configureView(){
        let scrollImg: UIScrollView = UIScrollView()
        scrollImg.delegate = self
        scrollImg.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height)
        scrollImg.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.90)
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
        view.addSubview(scrollImg)
        imgPhoto.layer.cornerRadius = 11.0
        imgPhoto.clipsToBounds = false
        scrollImg.addSubview(imgPhoto)
        
        view.bringSubviewToFront(btnBack)

    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgPhoto
    }
    
    fileprivate func loadData() {
        if let image = selectedTopTens.largeUrl{
            self.imgPhoto.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "placeholder.png"), options: [.progressiveDownload])
        }
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods


}
