//
//  SmartMobeTests.swift
//  SmartMobeTests
//
//  Created by Roshan Rijal on 7/12/19.
//  Copyright © 2019 Roshan Rijal. All rights reserved.
//

import XCTest
@testable import SmartMobe

class SmartMobeTests: XCTestCase {
    
    func test_login_without_username() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let _ = vc.view
        
        XCTAssertEqual("Search", vc.btnSearch.titleLabel?.text!)
    }
    
    

}
