//
//  CollectionTopTens.swift
//  SmartMobe
//
//  Created by Roshan Rijal on 7/12/19.
//  Copyright © 2019 Roshan Rijal. All rights reserved.
//

import UIKit
import SDWebImage

class CollectionTopTens: UICollectionViewCell {
    
    @IBOutlet weak var imgPhoto: UIImageView!
    
    func loadCell(topTens: TopTens){
        if let image = topTens.imageUrl{
            self.imgPhoto.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "placeholder.png"), options: [.progressiveDownload])
        }
    }
}
